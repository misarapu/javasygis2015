package pr06;

public class LiisuT6mbamineMassiiv {

	public static void main(String[] args) {

		// Võistlejate arvu küsimine ehk massiivi suuruse määramine
		System.out.print("Sisesta võistlejate arv: ");
		int liiskudeArv = TextIO.getlnInt();
		String[] nimekiri = new String[liiskudeArv];

		// Nimede küsimine ehk massiivi täitmine
		for (int i = 0; i < nimekiri.length; i++) {
			System.out.print("Sisesta võistleja: ");
			nimekiri[i] = TextIO.getlnString();
		}
		
		for (String item : nimekiri){
			System.out.println(item);
		}

		// Suvalise arvu meetodi välja kutsumine ja kaotaja väljakuulutamine
		int liisuNumber = Meetodid.suvalineT2isarvVahemikus(0, nimekiri.length - 1);
		System.out.println("Liisutõmbamise kaotas " + nimekiri[liisuNumber] + "!");

	}

}
