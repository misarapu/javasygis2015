package eksam;

//Leida etteantud reaalarvude maatriksis niisuguse rea indeks,
//milles on kõige vähem negatiivseid elemente.
public class LeiaMaatriksist {
    public static void main(String[] args) {
        int[][] matrix = new int[][]{{110, -2, 3}, {-40, 5, 600, -1}, {20, 80}, {1, 1, 1, 1, 1}};

        System.out.println(leiaMaatriksist(matrix));
    }

    public static int leiaMaatriksist(int[][] mat) {
        int[] neg = new int[mat.length];
        for (int i = 0; i < mat.length; i++) {
            int reasNeg = 0;
            for (int j = 0; j < mat[i].length; j++) {
                if (mat[i][j] < 0) {
                    reasNeg = reasNeg + 1;
                }
            }
            neg[i] = reasNeg;
        }

//        for (int i = 0; i < neg.length; i++) {
//            System.out.print(neg[i] + " ");
//        }

        int max = neg[0];
        for (int i = 0; i < neg.length; i++) {
            if (max < neg[i]) {
                max = i;
            }
        }
        return max;
    }
}
