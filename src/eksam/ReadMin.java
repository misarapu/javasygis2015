package eksam;

public class ReadMin {

	public static void main(String[] args) {
		int[][] matrix = new int[][] { { 110, 2, 3 }, { 40, 5, 600, 1 }, { 20, 80 } };
		for (int i = 0; i < matrix.length; i++) {
			System.out.print(reaMinid(matrix)[i] + " ");
		}
	}

	public static int[] reaMinid(int[][] matrix) {
		int[] miinimumid = new int[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			int min = matrix[i][0];
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] < min) {
					min = matrix[i][j];
				}
			}
			miinimumid[i] = min;
		}
		return miinimumid;
	}
}
