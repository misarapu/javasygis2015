package eksam;


// Koostada Java meetod, mis asendab parameetrina etteantud s�nes s k�ik t�hikud m�rgiga '-'.
public class SymboliAsendamine2 {
	public static void main(String[] args) {
		
		String s = "tyra raisk mine putsi";
		System.out.println(asenda(s));
		
		// v�i lihtsalt
		
		System.out.println(s.replaceAll(" ", "-"));
		
	}
	
	public static String asenda(String s6ne){
		String uusS6ne = "";
		for (int i = 0 ; i < s6ne.length(); i++){
			if (s6ne.charAt(i) == ' '){
				uusS6ne = uusS6ne + '-';
			}
			else{
				uusS6ne = uusS6ne + s6ne.charAt(i);
			}
		}
		return uusS6ne;
	}
}
