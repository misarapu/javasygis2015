package eksam;

// Koostage Java-meetod, mis leiab etteantud massiivi m elementide absoluutv22rtuste summa.
public class AbsSumma {
	public static void main(String[] args) {
		System.out.println(absSum(new double[] { 2.2, 3, -4.23, 5.2, -6 }));
	}

	public static double absSum(double[] array) {
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + Math.abs(array[i]);
		}
		return sum;
	}
}
