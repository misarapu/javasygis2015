package eksam;

// Koostage Java meetod etteantud täisarvumaatriksi m reasummade massiivi leidmiseks.
public class ReaSummad {
	public static void main(String[] args) {
		int[][] matrix = new int[][] { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 0 } };
		int[] summad = reaMaksimumid(matrix);
		for (int i = 0; i < summad.length; i++) {
			System.out.println(summad[i]);
		}
	}

	public static int[] reaMaksimumid(int[][] matrix) {
		int[] summad = new int[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			int sum = 0;
			for (int j = 0; j < matrix[i].length; j++) {
				sum = sum + matrix[i][j];
			}
			summad[i] = sum;
		}
		return summad;
	}
}
