package lihtsamadMeetodid;

public class MassiivM2rgiMuutumisteArv {
	public static void main(String[] args) {

	}

	public static int mmArv(int[] m) {
		int res = 0;
		if (m.length < 2)
			return 0;
		for (int i = 0; i < m.length - 1; i++) {
			if (m[i] >= 0 && m[i + 1] < 0)
				res++;
			if (m[i] < 0 && m[i + 1] >= 0)
				res++;
		}
		return res;
	} // mmArv

}
