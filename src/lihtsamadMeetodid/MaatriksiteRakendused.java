package lihtsamadMeetodid;

public class MaatriksiteRakendused {
	/** Peameetod silumiseks. */
	public static void main(String[] s) {
		int[][] a = new int[][] { { 5, 2, 4 }, { 0, 1, 5 }, { 1, 0, 1 } };
		System.out.println("Maatriks A: " + soneKuju(a));
		int[][] b = new int[][] { { 2, 8, 0 }, { 5, 3, 1 }, { 7, 4, 6 } };
		System.out.println("Maatriks B: " + soneKuju(b));
		int[][] c = summa(a, b);
		System.out.println("A + B: " + soneKuju(c));
		int[][] d = korrutis(a, b);
		System.out.println("A * B: " + soneKuju(d));
		System.out.println("B minReaMax on: " + minReaMax(b));
		System.out.println("B maxReaMin on: " + maxReaMin(b));
		System.out.println("B minVeeruMax on: " + minVeeruMax(b));
		System.out.println("B maxVeeruMin on: " + maxVeeruMin(b));
		if (minReaMax(a) == maxVeeruMin(a))
			System.out.println("A alumine sadulpunkt on: " + minReaMax(a));
		else
			System.out.println("A-l ei ole alumist sadulpunkti");
		if (maxReaMin(a) == minVeeruMax(a))
			System.out.println("A ylemine sadulpunkt on: " + maxReaMin(a));
		else
			System.out.println("A-l ei ole ylemist sadulpunkti");
	} // main

	/** Teisendus s6neks. */
	public static String soneKuju(int[][] m) {
		String nl = System.getProperty("line.separator");
		StringBuffer sb = new StringBuffer(nl);
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				sb.append(String.valueOf(m[i][j]));
				sb.append("\t");
			}
			sb.append(nl);
		}
		return sb.toString();
	} // soneKuju

	/** Maatriksite liitmine. */
	public static int[][] summa(int[][] m1, int[][] m2) {
		if (m1.length != m2.length)
			throw new RuntimeException("ei saa liita");
		int[][] res = new int[m1.length][];
		for (int i = 0; i < m1.length; i++) {
			if (m1[i].length != m2[i].length)
				throw new RuntimeException("ei saa liita");
			res[i] = new int[m1[i].length];
			for (int j = 0; j < m1[i].length; j++) {
				res[i][j] = m1[i][j] + m2[i][j];
			}
		}
		return res;
	} // summa

	/** Maatriksite korrutamine. */
	public static int[][] korrutis(int[][] m1, int[][] m2) {
		int m = m1.length;
		int p = m1[0].length;
		int n = m2[0].length;
		if (p != m2.length)
			throw new RuntimeException("Ei saa korrutada");
		int[][] res = new int[m][n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				res[i][j] = 0;
				for (int k = 0; k < p; k++) {
					res[i][j] += m1[i][k] * m2[k][j];
				}
			}
		}
		return res;
	} // korrutis

	/** Maksimaalne element ridade miinimumide hulgas. */
	public static int maxReaMin(int[][] m) {
		int result = Integer.MIN_VALUE; // l6pptulemuse algv22rtus
		// Tsykkel yle ridade
		for (int i = 0; i < m.length; i++) {
			int rMin = Integer.MAX_VALUE; // jooksva rea miinimum
			for (int j = 0; j < m[i].length; j++) {
				if (rMin > m[i][j])
					rMin = m[i][j];
			} // for j
			if (result < rMin)
				result = rMin;
		} // for i
		return result;
	} // maxReaMin

	/** Minimaalne element ridade maksimumide hulgas. */
	public static int minReaMax(int[][] m) {
		int result = Integer.MAX_VALUE; // l6pptulemuse algv22rtus
		// Tsykkel yle ridade
		for (int i = 0; i < m.length; i++) {
			int rMax = Integer.MIN_VALUE; // jooksva rea maksimum
			for (int j = 0; j < m[i].length; j++) {
				if (rMax < m[i][j])
					rMax = m[i][j];
			} // for j
			if (result > rMax)
				result = rMax;
		} // for i
		return result;
	} // minReaMax

	/** Maksimaalne element veergude miinimumide hulgas. */
	public static int maxVeeruMin(int[][] m) {
		int result = Integer.MIN_VALUE; // l6pptulemuse algv22rtus
		// Tsykkel yle veergude
		for (int j = 0; j < m[0].length; j++) {
			int vMin = Integer.MAX_VALUE; // jooksva veeru miinimum
			for (int i = 0; i < m.length; i++) {
				if (vMin > m[i][j])
					vMin = m[i][j];
			} // for i
			if (result < vMin)
				result = vMin;
		} // for j
		return result;
	} // maxVeeruMin

	/**
	 * Minimaalne element veergude maksimumide hulgas. Lubatud erineva pikkusega
	 * read.
	 */
	public static int minVeeruMax(int[][] m) {
		int result = Integer.MAX_VALUE; // l6pptulemuse algv22rtus
		// teeme kindlaks maksimaalse reapikkuse vArv
		int vArv = 0;
		for (int i = 0; i < m.length; i++) {
			if (vArv < m[i].length)
				vArv = m[i].length;
		}
		// hoiame veergude maksimume massiivis vMax
		int[] vMax = new int[vArv];
		for (int j = 0; j < vArv; j++)
			vMax[j] = Integer.MIN_VALUE;
		// arvutame maksimumid
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				if (vMax[j] < m[i][j])
					vMax[j] = m[i][j];
			}
		}
		// leiame minimaalse
		for (int j = 0; j < vArv; j++) {
			if (vMax[j] < result)
				result = vMax[j];
		} // for j
		return result;
	} // minVeeruMax
}

public class Maatriks {

	/** Peameetod muude testimiseks. */
	public static void main(String[] a) {
		Maatriks m1 = new Maatriks(new int[][] { { 4, 9, 2 }, { 0, 7, 4 } });
		Maatriks m2 = new Maatriks(3, 4);
		m2.set(0, 0, 1);
		m2.set(1, 1, 1);
		m2.set(2, 2, 1);
		Maatriks m3 = null;
		System.out.println(m1);
		System.out.println(m2);
		System.out.println(m1.korda(m2));
		try {
			m3 = (Maatriks) m1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		m1.set(0, 0, 1);
		System.out.println(m1.pluss(m3));

		// v6rdsuse ja identsuse testid
		Maatriks p1 = new Maatriks(new int[][] { { 1, 2 }, { 3, 4 } });
		Maatriks p2 = p1;
		System.out.println(p2.equals(p1)); // true
		System.out.println(p2 == p1); // true
		p2.set(0, 0, 7);
		System.out.println(p2.equals(p1)); // true
		System.out.println(p2 == p1); // true
		try {
			p2 = (Maatriks) p1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println(p2.equals(p1)); // true
		System.out.println(p2 == p1); // false
		p2.set(0, 0, 5);
		System.out.println(p2.equals(p1)); // false
		System.out.println(p2 == p1); // false

	} // main

	/** maatriksi sisu */
	private int[][] massiiv;

	/** Nullmaatriksi konstruktor. */
	Maatriks(int ridu, int veerge) {
		if (ridu < 1)
			massiiv = null;
		else if (veerge < 1)
			massiiv = null;
		else {
			// m2lu eraldamine
			massiiv = new int[ridu][veerge];
			// massiivi nullimine
			for (int i = 0; i < massiiv.length; i++) {
				for (int j = 0; j < massiiv[i].length; j++) {
					massiiv[i][j] = 0;
				}
			}
		} // else
	} // konstruktor

	/** Maatriksi konstruktor etteantud massiivist. */
	Maatriks(int[][] mat) {
		if (mat == null)
			massiiv = null;
		else if (mat.length < 1)
			massiiv = null;
		else if (mat[0].length < 1)
			massiiv = null;
		else {
			massiiv = new int[mat.length][mat[0].length];
			for (int i = 0; i < mat.length; i++) {
				for (int j = 0; j < mat[0].length; j++) {
					massiiv[i][j] = mat[i][j];
				}
			}
		} // else
	} // konstruktor

	/** Elemendi kirjutamine maatriksisse. */
	public int set(int i, int j, int element) {
		int res = massiiv[i][j];
		massiiv[i][j] = element;
		return res;
	} // set

	/** Elemendi lugemine maatriksist. */
	public int get(int i, int j) {
		return massiiv[i][j];
	} // get

	/** Teisendus s6neks. */
	public String toString() {
		StringBuffer res = new StringBuffer();
		String nl = System.getProperty("line.separator");
		for (int i = 0; i < massiiv.length; i++) {
			for (int j = 0; j < massiiv[0].length; j++) {
				res.append(String.valueOf(massiiv[i][j]) + "\t");
			}
			res.append(nl);
		}
		return res.toString();
	} // toString

	/** Koopia loomine. */
	public Object clone() throws CloneNotSupportedException {
		return new Maatriks(massiiv);
	} // clone

	/** Kahe maatriksi v6rdsuse tuvastamine. */
	public boolean equals(Object o) {
		if (o == null)
			return false;
		int[][] om = ((Maatriks) o).massiiv;
		if (massiiv.length != om.length)
			return false;
		for (int i = 0; i < massiiv.length; i++) {
			if (massiiv[i].length != om[i].length)
				return false;
			for (int j = 0; j < massiiv[i].length; j++) {
				if (massiiv[i][j] != om[i][j])
					return false;
			}
		}
		return true;
	} // equals

	/** Maatriksite korrutamine. */
	public Maatriks korda(Maatriks mat) {
		if (mat == null)
			return null;
		if (massiiv[0].length != mat.massiiv.length)
			throw new RuntimeException("valed m66tmed korrutamisel");
		Maatriks res = new Maatriks(massiiv.length, mat.massiiv[0].length);
		for (int i = 0; i < massiiv.length; i++) {
			for (int j = 0; j < mat.massiiv[0].length; j++) {
				res.massiiv[i][j] = 0;
				for (int k = 0; k < mat.massiiv.length; k++) {
					res.massiiv[i][j] += massiiv[i][k] * mat.massiiv[k][j];
				}
			}
		}
		return res;
	} // korda

	/** Maatriksite liitmine (vajadusel t2iendatud nullidega). */
	public Maatriks pluss(Maatriks mat) {
		if (mat == null)
			return new Maatriks(massiiv);
		Maatriks res = new Maatriks(Math.max(massiiv.length, mat.massiiv.length),
				Math.max(massiiv[0].length, mat.massiiv[0].length));
		for (int i = 0; i < massiiv.length; i++) {
			for (int j = 0; j < massiiv[i].length; j++) {
				res.massiiv[i][j] = massiiv[i][j];
			}
		}
		for (int i = 0; i < mat.massiiv.length; i++) {
			for (int j = 0; j < mat.massiiv[i].length; j++) {
				res.massiiv[i][j] += mat.massiiv[i][j];
			}
		}
		return res;
	} // pluss

	public static void transponeeri(double[][] maatriks) {
		if (maatriks == null)
			return;
		int x = maatriks.length;
		if (x == 0)
			throw new IllegalArgumentException("tyhi maatriks!");
		int y = maatriks[0].length;
		if (y != x)
			throw new IllegalArgumentException("ei ole ruutmaatriks!");

		double tmp; // vahetamiseks
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < i; j++) {
				tmp = maatriks[i][j];
				maatriks[i][j] = maatriks[j][i];
				maatriks[j][i] = tmp;
			}
		}
	} // transponeeri

	/** Transponeerimine uude maatriksisse. */
	public static double[][] transp(double[][] maatriks) {
		if (maatriks == null)
			return null;
		int x = maatriks.length;
		if (x == 0)
			throw new IllegalArgumentException("tyhi maatriks!");
		int y = maatriks[0].length;
		if (y == 0)
			throw new IllegalArgumentException("tyhi rida!");

		double[][] result = new double[y][x];
		for (int i = 0; i < y; i++) {
			for (int j = 0; j < x; j++) {
				result[i][j] = maatriks[j][i];
			}
		}
		return result;
	} // transp

} // Maatriks
