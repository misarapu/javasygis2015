package lihtsamadMeetodid;

public class Proov {
//	Programmeerimiskeele süntaksit kirjeldab...
//	a. testide hulk
//	b. grammatika
//	c. API 
//	d. IDE b
//	
//	Milline alljärgnevatest keeltest esindab objektorienteeritud programmeerimise paradigmat? 
//	a. Haskell
//	b. Ada
//	c. Prolog 
//	d. Smalltalk d
//	
//	Milline alljärgnevatest keeltest esindab imperatiivse programmeerimise paradigmat?
//	a. Ada
//	b. Haskell
//	c. Smalltalk
//	d. Prolog a
//	
//	Milline alljärgnevatest keeltest esindab loogilise programmeerimise paradigmat? 
//	a. Haskell 
//	b. Smalltalk 
//	c. Ada 
//	d. Prolog d
//	
//	Milline järgnevatest tüüpidest EI OLE algtüüp (primitive type)?
//	a. boolean
//	b. double
//	c. Long 
//	d. float C
//	
//	Milline järgnevatest keele Java tüüpidest on algtüüp (primitive type)?
//	a. Long
//	b. boolean
//	c. Double
//	d. Integrersaq B
//	
//	Mida tähistab keeles Java järgmine kirjutis? -0xa
//	a. Ei ole Javas lubatud
//	b. 16-bitine täisarv -10
//	c. 32-bitine täisarv -10
//	d. 64-bitine täisarv -10 c
//	
//	Mida tähistab keeles Java järgmine kirjutis? -0.5e-2 
//	a. 32-bitine reaalarv -0.005
//	b. 64-bitine reaalarv -50.00
//	c. 64-bitine reaalarv -0.05
//	d. 64-bitine reaalarv -0.005 d
//	
//	Mida tähistab keeles Java järgmine kirjutis? -0.5e-2f 
//	a. 32-bitine reaalarv -0.05 
//	b. 32-bitine reaalarv -0.005
//	c. 64-bitine reaalarv -0.005
//	d. 32-bitine reaalarv -50.00 b
//	
//	Millise tulemuse annab järgmine programmilõik? 
//	int a = 9; 
//	int b = ++a / 5; 
//	a. a==10 && b==1 
//	b. a==9 && b==1 
//	c. a==9 && b==2 
//	d. a==10 && b==2 d
//	
//	Millise tulemuse annab järgmine programmilõik? 
//	[code java]
//	int a = 4;
//	int b = a++ / 5;
//	[/code]
//	a. a==5 && b==0.8
//	b. a==4 && b==0
//	c. a==5 && b==1
//	d. a==5 && b==0 d
//	
//	Mida väljendab alljärgneva programmilõigu teine lause? 
//	String s = "Hello"; 
//	int len = s.length(); 
//	a. klassi String klassimeetodi poole pöördumist 
//	b. jooksva klassi isendimeetodi poole pöördumist
//	c. klassi Integer konstruktori poole pöördumist
//	d. objekti s isendimeetodi poole pöördumist a
//	
//	Milline tingimus kirjeldab n mittekuulumist poollõiku [0, 10) ? 
//	a. (n >= 0) && (n < 10)
//	b. (n <0) || (n >= 10)
//	c. (n >=0) || (n < 10)
//	d. (n < 0) && (n >= 10) b
//	
//	Võtmesõna "while" järele ümarsulgudesse kirjutatav tingimus on... 
//	a. Java virtuaalmasina peatamise tingimus
//	b. tsükli lõpetamise tingimus
//	c. meetodist väljumise tingimus
//	d. tsükli jätkamise tingimus d
//	
//	Käsu return järele kirjutatud avaldisega määratakse: 
//	a. tegelikud parameetrid
//	b. formaalsed parameetrid
//	c. signatuur
//	d. tagastusväärtus d
//	
//	Java return lause...
//	a. annab juhtimise tagasi meetodi väljakutsujale
//	b. tekitab erindi (vea)
//	c. ei tee midagi (tühilause)
//	d. väljastab teksti a
//	
//	Meetodi nimi koos piiritlejate, parameetrite tüüpide ning tagastusväärtuse tüübiga on tuntud kui meetodi:
//	a. tegelikud parameetrid 
//	b. signatuur
//	c. formaalsed parameetrid
//	d. tagastusväärtus b
//	
//	Java rakendus (application) peab sisaldama meetodit, mille signatuur on... 
//	a. public static int main (String[] ) 
//	b. public void main (String ) 
//	c. public void run() 
//	d. public static void main (String[] ) d
//	
//	Millist tüüpi väärtuse tagastab meetod m?
//	public static int m (double d, String s);
//	a. int
//	b. double
//	c. String
//	d. void a
//	
//	Javadoc @version abil dokumenteeritakse: 
//	a. klassi versiooniinfot
//	b. meetodi tagastusväärtust
//	c. meetodi parameetrit
//	d. meetodi poolt tekitatavaid erindeid a
//	
//	Javadoc @since abil dokumenteeritakse:
//	a. klassi versiooniinfot
//	b. hetkel kasutatavat Java kompilaatori versiooni
//	c. Java versiooni, millest alates seda klassi ei toetata
//	d. varaseimat Java kompilaatori versiooni, millega klass kompileerub d
//	
//	Javadoc @param abil dokumenteeritakse:
//	a.meetodi parameetrit
//	b.versiooniinfot
//	c.meetodi tagastusväärtust
//	d.meetodi poolt tekitavaid erindeid a
//	
//	Mida väljendab alljärgnv programmilõik?
//	String s=String valueOf(1234); objekti s isendi poole pöördumist
//	
//	Millist tüüpi väärtuse tagastab meetod m?
//	public static String m (double d, int i);
//	a. int
//	b. double
//	c. String
//	d. void c
//	
//	Javadoc @return abil dokumenteeritakse: 
//	a. klassi versiooniinfot
//	b. meetodi tagastusväärtust
//	c. meetodi parameetrit
//	d. meetodi poolt tekitatavaid erindeid b
//	
//	Java koodistiil nõuab konstantide nimede kirjutamist:
//	a. ainult suurtähtedega
//	b. kreeka keeles
//	c. suure algustähega 
//	d. väikese algustähega d
//	
//	Java koodistiil nõuab klassinimede kirjutamist:
//	a.suure algustähega
//	b. ainult väikeste tähtedega
//	c. väikese algustähega
//	d. ainult suurtähtedega a
//	
//	Java koodistiil nõuab muutujate nimede kirjutamist:
//	a.suure algustähega
//	b. ainult väikeste tähtedega
//	c. väikese algustähega
//	d. ainult suurtähtedega a
//	
//	Teate saatmine objektile tähendab:
//	a. objektile mälu eraldamist
//	b. pöördumist vastava isendimeetodi poole
//	c. objektile uue stringiesituse määramist
//	d. pöördumist vastava klassimeetodi poole b
//	
//	Objekti (isendi) identiteedi Javas määrab:
//	a. mäluaadress
//	b. objekti esitus sõne kujul
//	c. isendimuut ujate väärtuste hulk
//	d. klassimuutujate väärtuste hulk a
//	
//	Destruktor on:
//	a. dünaamilise pikkusega andmestruktuur
//	b. meetod isendi hävitamiseks, keeles Java puudub
//	c. isendimeetod, mis teisendab objekti stringiks
//	d. meetod isendi loomiseks b
//	
//	Konstruktor on meetod isendi loomiseks
//	
//	Liides List keeles Java võimaldab: 
//	a. küsida andmekogumi järgmist elementi
//	b. salvestada paare "võti-väärtus" 
//	c. otsustada, kumb kahest elemendist on suurem
//	d. salvestada andmestruktuuri korduvaid elemente, mida eristatakse indeksi järgi d
//	
//	Liides Comparable keeles Java võimaldab:
//	a. salvestada paare "võti-väärtus"
//	b. lisada hulka korduvaid elemente
//	c. otsustada, kumb kahest elemendist on suurem
//	d. küsida andmekogumi järgmist elementi c
//	
//	Liides Map keeles Java võimaldab:
//	a. salvestada paare "võti-väärtus"
//	b. lisada hulka korduvaid elemente
//	c. otsustada, kumb kahest elemendist on suurem
//	d. küsida andmekogumi järgmist elementi a
//	
//	Java break lause... 
//	a. annab juhtimise tagasi meetodi väljakutsujale
//	b. lõpetab Java virtuaalmasina töö
//	c. lõpetab sisemise tsükli sammu
//	d. lõpetab sisemise tsükli või valikulause täitmise d
//	
//	Paaride "võti - väärtus" salvestamiseks sobib Java APIs klass: 
//	a. TreeSet 
//	b. ArrayList 
//	c. HashMap 
//	d. Iterator c
//	
//	Paisktabelit esindab Java APIs liides:
//	a. Collection
//	b. Map
//	c. Comparable
//	d. Set b
//	
//	Millise eriolukorra (loetletutest) tuvastamine ja töötlemine on Javas mõttekas: 
//	a. ThreadDeath 
//	b. Exception
//	c. Error 
//	d. OutOfMemoryError b
//	
//	Eriolukorda, mille töötlemine programmeerija poolt on Javas kohustuslik, nimetatakse:
//	a. checked exception
//	b. RuntimeException 
//	c. Error
//	d. Exception c
//	
//	Eriolukorda, mille töötlemine programmeerija poolt ei ole Javas kohustuslik, nimetatakse:
//	a. checked exception
//	b. RuntimeException 
//	c. Error
//	d. Exception b
//	
//	Baidivoo lugemisel meetodiga read() annab voo lõppemisest märku:
//	a. (String)null
//	b. (int)-1 
//	c. (byte)-128
//	d. (int)0 b
//	
//	Tekstivoo lugemisel meetodiga read() annab voo lõppemisest märku:
//	a. (String)null
//	b. (int)-1 
//	c. (byte)-128
//	d. (int)0 a
//	
//	Millist kasutajaliidese aspekti juhib paigutushaldur?
//	a. liidese visuaalne kuju
//	b. liidese mudel
//	c. liidese seos välise keskkonnaga
//	d. liidese reageerimine sündmustele a
//	
//	Ühene pärimine tähendab, et: 
//	a. teate poolt aktiviseeritav meetod valitakse programmi lahendamise ajal 
//	b. teate poolt aktiviseeritav meetod valitakse programmi kompileerimise ajal 
//	c. klass võib omadusi pärida mitmelt ülemklassilt 
//	d. iga klass saab olla ülimalt ühe klassi alamklass d
//	
//	Mitmene pärimine tähendab, et:
//	Multiple inheritance means that
//
//	a. iga klass saab olla ülimalt ühe klassi alamklass; each class can be a subclass of at most one class
//
//	b. klass võib omadusi pärida mitmelt ülemklassilt; a class can inherit properties from several superclasses 
//
//	c. teate poolt aktiviseeritav meetod valitakse programmi lahendamise ajal; the method activated by a message is chosen during runtime
//
//	d. teate poolt aktiviseeritav meetod valitakse programmi kompileerimise ajal; the method activated by a message is chosen during compile time b
//	
//	Rakendi kuva värskendamist saab tellida pöördudes meetodi ... poole.
//	a. start
//	b. repaint
//	c. paint Vale
//	d. init b
//	
//	Rakendi passiivseks muutumisel pöördutakse meetodi ... poole. 
//	a. stop
//	b. start 
//	c. repaint 
//	d. init a
//	
//	Rakendi elutsükli alguses täidetakse meetod:
//	a. start 
//	b. init
//	c. repaint
//	d. paint b
//	
//	Milline käsk joonistab ringi (pinna) läbimõõduga 50 pikselit?
//	a. g.drawOval (50, 50, 50, 50);
//	b. g.drawOval (0, 0, 100, 100);
//	c. g.fillOval (0, 0, 100, 100);
//	d. g.fillOval (50, 50, 50, 50); d
//	
//	Milline käsk joonistab ringjoone läbimõõduga 50 pikselit? 
//	a. g.fillOval (50, 50, 50, 50); 
//	b. g.fillOval (0, 0, 100, 100); 
//	c. g.drawOval (0, 0, 100, 100); 
//	d. g.drawOval (50, 50, 50, 50); d
//	
//	Milline käsk joonistab ringjoone raadiusega 50 pikselit?
//	a.g.fillOval (50, 50, 50, 50);
//	b. g.drawOval (50, 50, 50, 50);
//	c. g.drawOval (0, 0, 100, 100); c
//	
//	Java algtüüpidesse kuuluvate andmete sisestamiseks on ette nähtud meetodid klassis: 
//	a. DataInputStream 
//	b. ByteArrayInputStream 
//	c. File 
//	d. FileReader a
//	
//	Java algtüüpidesse kuuluvate andmete väljastamimseks on ette nähtud meetodid klassis:
//	a. FileWriter
//	b. ByteArrayOutputStream
//	c. File
//	d. DataOutputStream d
//	
//	Millist kasutajaliidese aspekti juhivad kuularid? a.liidese reageerimine sündmustele
//	
//	Java programmi lähtekood on failis laiendiga? .java
//	
//	olgu muutuja m kelles Java kirjeldatud kui täisarvude massiiv. Mälu eraldamiseks massiivi m 10 elemendi jaoks tuleb kirjutada:
//	a. m=new int[10]
//	b.m.length=10
//	c.m[9]=0;
//	d.m.allocate a
//	
//	java continue lause lõpetab sisemise tsükli sammu
//	
//	java programmi transleerimisel tekkiv baitkood on failis laiendiga .class
//	
//	Kahe elemendi omavahelise järjestuse määramiseks sobib java APIs liides:
//	a)Collection
//	b)Comparable
//	c)Map
//	d)Set b
//	
//	Meetodi päises kkirjutatud muutujad on 
//	a. tagastusväärtus
//	b. tegelikud väärtused
//	c. formaalsed parameetrid
//	d. siganatuur b
//	
//	iteraator võimaldab:
//	a. otsustada, kumb kahest elemendist on suurem
//	b. salvestada paare"võti-väärtus"'
//	c.lisada hulka korduvaid elemente
//	d. küsida andmekogumi järgmist elementi d
//	
//	Korduvate elementideta järjestatud hulka esindab java APIs klass:
//	a. ArrayList
//	b. LinkList
//	c TreeSet
//	d. Iteraator a
//	
//	Isendimuutujate väärtused määravad objekti:
//	Values of instance variables determine:
//
//	1. oleku; object state
//	2. identiteedi; object identity
//	3. klassi; object class
//	4. käitumise; object behaviour 1
//	
//	Hiline seostamine tähendab,et 
//	a.teate poolt aktiviseeritav meetod valib programmi lahendamise ajal
//	b.klass võib omadusi pärida mitmelt ülemklassilt
//	c. iga klass saab olla ülimalt ühe klassi alamklass
//	d.teate poolt aktiviseeritav meetod valitakse programmi kompileerimise ajal d
//	
//	Paaride" võti-väärtus" salvesatamiseks sobib Java APIs klass:
//	a. TreeSet
//	b. ArrayList
//	c. HashMap
//	d. Iterator
//	Mis tähistab baidivoo lõppu? * -1

}
