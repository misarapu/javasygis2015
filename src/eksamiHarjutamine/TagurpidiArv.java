package eksamiHarjutamine;

// On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab täisarvu, 
// mis saadakse n kümnendesituses numbrite järjekorra ümberpööramise teel.
public class TagurpidiArv {
	public static void main(String[] args) {
		System.out.println(tagurpidi(123));
	}

	public static int tagurpidi(int arv) {
		int sum = 0;
		do {
			int j22k = arv % 10;
			sum = sum * 10 + j22k;
			arv = arv / 10;
		} while (arv > 0);
		return sum;
	}
}
