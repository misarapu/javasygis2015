package pr05;

public class LiisuT6mbamine {
	public static void main(String[] args) {
		
		// Liisutõmbamises osalevate inimeste arvu küsimine
		System.out.print("Sisesta inimeste arv: ");
		int inimesteArv = TextIO.getlnInt();
		
		// Meetodi "suvlineT2isarvVahemikus" kasutamine
		int v2ljaValitu = Meetodid.suvalineT2isarvVahemikus("", 1, inimesteArv);
		
		System.out.print("Väljavalitud on " + v2ljaValitu + ". inimene.");

	}
}