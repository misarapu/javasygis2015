package pr08;

import java.util.ArrayList;

public class InimesteSisestamine {

	public static void main(String[] args) {
		// String nimi = new String("Mati");
		//
		// // uus Inimene tüüpi objekt
		// Inimene keegi = new Inimene("Kati", 24);
		//
		// keegi.tervita();
		//
		// Inimene keegiVeel = new Inimene("Juri", 23);
		//
		//

		String nimi = "";
		int vanus = 0;
		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();
		while (true) {

			System.out.print("Sisesta nimi: ");
			nimi = TextIO.getlnString();
			// Kui nimeks panna "exit", siis lõpetatakse nimeda küsimine
			if (nimi.equals("exit")) {
				break;
			}
			System.out.print("Sisesta selle inimese vanus: ");
			vanus = TextIO.getlnInt();
			inimesed.add(new Inimene(nimi, vanus));
		}

		for (Inimene inimene : inimesed) {
			// Java kutsub v�lja Inimene klassi toString() meetodi
			System.out.println(inimene);
			inimene.tervita();
		}

	}

}
