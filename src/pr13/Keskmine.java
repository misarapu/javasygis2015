package pr13;

import java.util.ArrayList;

public class Keskmine {

	public static void main(String[] args) {

		String kataloogitee = Keskmine.class.getResource(".").getPath();
		ArrayList<String> failiSisu = FailistLugemine.loeFail(kataloogitee + "numbrid.txt");
		System.out.println(failiSisu);

		// String -> Double ja numbrite summa leidmine
		double sum = 0;
		double nr;
		int VigaseidRidu = 0;
		for (int i = 0; i < failiSisu.size(); i++) {
			try {
				nr = Double.parseDouble(failiSisu.get(i));
				sum = sum + nr;
			} catch (NumberFormatException e) {
				System.out.println("Vigane rida: " + i);
				VigaseidRidu++;
			}
		}
		
		//Kesmine
		double keskmine;
		keskmine = sum / (failiSisu.size() - VigaseidRidu);
		System.out.println(keskmine);

	}

}
