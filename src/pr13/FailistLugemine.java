package pr13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FailistLugemine {
	
	public static ArrayList<String> loeFail(String failinimi){
		
		
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(failinimi);
		
		ArrayList<String> nimekiri = new ArrayList<String>(); 
		
		try {
		    // avame faili lugemise jaoks
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				nimekiri.add(rida);
			}
		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
		
		return nimekiri;
		
	}
	
	
	public static void main(String[] args) {
		
	    // punkt tähistab jooksvat kataloogi
	    String kataloogitee = FailistLugemine.class.getResource(".").getPath();
	    System.out.println(kataloogitee);
	    ArrayList<String> failiSisu = loeFail(kataloogitee + "kala.txt");
	    System.out.println(failiSisu);
	    Collections.sort(failiSisu);
	    System.out.println(failiSisu);
	}
}